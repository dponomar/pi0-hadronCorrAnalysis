const char *anatype = "AOD";

void lego_train()
{
// Analysis using AOD data
// Automatically generated analysis steering macro executed in grid subjobs

   TStopwatch timer;
   timer.Start();

// Set temporary merging directory to current one
   gSystem->Setenv("TMPDIR", gSystem->pwd());

// Set temporary compilation directory to current one
   gSystem->SetBuildDir(gSystem->pwd(), kTRUE);

// Reset existing include path and add current directory first in the search
   gSystem->SetIncludePath("-I.");
// load base root libraries
   gSystem->Load("libTree");
   gSystem->Load("libGeom");
   gSystem->Load("libVMC");
   gSystem->Load("libPhysics");

   gSystem->Load("libMinuit");

// Add aditional libraries
   gSystem->Load("libVMC");
   gSystem->Load("libPhysics");
   gSystem->Load("libTree");
   gSystem->Load("libMinuit");
   gSystem->Load("libProof");
   gSystem->Load("libSTEERBase");
   gSystem->Load("libESD");
   gSystem->Load("libAOD");
// Load analysis framework libraries
   gSystem->Load("libSTEERBase");
   gSystem->Load("libESD");
   gSystem->Load("libAOD");
   gSystem->Load("libANALYSIS");
   gSystem->Load("libANALYSISalice");
   gSystem->Load("libOADB");
   gSystem->Load("libCORRFW");

// include path
   TString intPath = gInterpreter->GetIncludePath();
   TObjArray *listpaths = intPath.Tokenize(" ");
   TIter nextpath(listpaths);
   TObjString *pname;
   while ((pname=(TObjString*)nextpath())) {
      TString current = pname->GetName();
      if (current.Contains("AliRoot") || current.Contains("ALICE_ROOT")) continue;
      gSystem->AddIncludePath(current);
   }
   if (listpaths) delete listpaths;
   gROOT->ProcessLine(".include $ALICE_ROOT/include");
   printf("Include path: %s\n", gSystem->GetIncludePath());

// Add aditional AliRoot libraries
   gSystem->Load("libTree");
   gSystem->Load("libVMC");
   gSystem->Load("libGui");
   gSystem->Load("libXMLParser");
   gSystem->Load("libMinuit");
   gSystem->Load("libMinuit2");
   gSystem->Load("libProof");
   gSystem->Load("libGeom");
   gSystem->Load("libPhysics");
   gSystem->Load("libSTEERBase");
   gSystem->Load("libESD");
   gSystem->Load("libAOD");
   gSystem->Load("libOADB");
   gSystem->Load("libANALYSIS");
   gSystem->Load("libANALYSISalice");
   gSystem->Load("libCDB");
   gSystem->Load("libRAWDatabase");
   gSystem->Load("libSTEER");
   gSystem->Load("libCORRFW");
   gSystem->Load("libPWGflowBase");
   gSystem->Load("libPWGflowTasks");
   gSystem->Load("libPWGGAGammaConv");
   gSystem->Load("libCore");
   gSystem->Load("libPHOSUtils");
   gSystem->Load("libPHOSbase");
   gSystem->Load("libPHOSpi0Calib");
   gSystem->Load("libPHOSrec");
   gSystem->Load("libPHOSshuttle");
   gSystem->Load("libPHOSsim");
   gSystem->Load("libPWGGAPHOSTasks");
   gSystem->Load("libTender");
   gSystem->Load("libTRDbase");
   gSystem->Load("libVZERObase");
   gSystem->Load("libVZEROrec");
   gSystem->Load("libTenderSupplies");

// analysis source to be compiled at runtime (if any)

// read the analysis manager from file
   AliAnalysisManager *mgr = AliAnalysisAlien::LoadAnalysisManager("lego_train.root");
   if (!mgr) return;
   AliAnalysisAlien *plugin = new AliAnalysisAlien();
   plugin->SetRunMode("test");
   plugin->SetFileForTestMode(".//__alice__data__2013__LHC13b__000195483__ESDs__pass3__AOD154__0004_root_archive_AliAOD_2.txt");
   plugin->SetNtestFiles(2);
   mgr->SetGridHandler(plugin);
   mgr->SetDebugLevel(1);
   mgr->SetNSysInfo(100);
   mgr->PrintStatus();
   AliLog::SetGlobalLogLevel(AliLog::kError);
   mgr->StartAnalysis("localfile");
   timer.Stop();
   timer.Print();
}

