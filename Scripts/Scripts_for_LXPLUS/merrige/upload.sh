#! /bin/bash
mkdir sum

for run in `cat runs.txt`
do
  echo "$(tput setaf 2)Processing run " $run; echo "$(tput sgr0)"
 # scp  dponomar@lxplus.cern.ch:/afs/cern.ch/work/d/dponomar/MonALISA/merrige/collection.xml .
#      /afs/cern.ch/alice/alien2/pro/api/bin/alien_find -x collection /alice/cern.ch/user/d/dponomar/Correlation/output/$run AnalysisResults.root > collection.xml
#  /afs/cern.ch/alice/alien2/pro/api/bin/alien_find -x collection /alice/cern.ch/user/d/dblau/Correlations/outputPbPb/$run tof.root > collection.xml
# /afs/cern.ch/alice/alien2/pro/api/bin/alien_find -x collection /alice/cern.ch/user/d/dponomar/Correlation/outputMerge/ sum.root > collection.xml 

  myvar=0
  while [ $myvar -ne $1 ]
  do
      echo "$(tput setaf 2)Processing $myvar ";  echo "$(tput sgr0)"
       root -q -b addhisto_xml.C"($myvar)" #>& log/add_${run}_0.log
      myvar=$(( $myvar + 1 ))
  done

root -q -b mergeall.C"($1)" #>& log/merge_${run}.log
  echo ".... merged"
#  /afs/cern.ch/alice/alien2/pro/api/bin/alien_cp file:/tmp/dblau/histos.root alien:/alice/cern.ch/user/d/dblau/sum_Run.root
cp histos.root sum/$run.root
  echo "uploaded"
  rm *.root >& /dev/null
#  rm collection.xml >& /dev/null
done
