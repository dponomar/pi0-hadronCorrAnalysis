#!/bin/bash
# $1    $2      $3    $4   
# $file $output $user $procDir
root -b -q -x mergerootfile.C --path "$4" --name "$1" --out "$2" --user "$3"
