#include <Scripts/GetRunInfo.C>

#include <Scripts/Angle/2D/FhdPhidEtaDistribution.C>
#include <Scripts/Angle/2D/FhdPhidEtaDistributionCorrected.C>

#include <Scripts/Angle/2D/FhdPhidEtaDistributionProjEta.C>
#include <Scripts/Angle/2D/FhdPhidEtaDistributionCorrectedProjEta.C>

#include <Scripts/Mass/FhMass_RealMix.C>


void StartAnalise4PhiEta(const string run = "pPb195482AOD139", const TString pi0min = "7", const TString pi0max = "20")
{
	StartAnalise(run, pi0min, pi0max );
}


Calculate(	const TSystemFile * systemFile, 
			const Double_t ptPi0min = 7., 
			const Double_t ptPi0max = 20.)
// Запуск всех функций. Настоятельно рекомендую удалить пустые root файлы из рабочей директории и запастись кофе с печеньками.
{
	PrintInTerminal_Yellow(Form("Correlations Pi0's pT range is from %2.1f to %2.1f", ptPi0min, ptPi0max));

	const Pi0GlobalInfo pi0GlobalInfo;
	pi0GlobalInfo.ptMin = ptPi0min;
	pi0GlobalInfo.ptMax = ptPi0max;

	if( !(TString(systemFile->GetName()).Contains("Default")) ) continue;
	if( !TestRootFilieQuality(systemFile) )						continue;

	// if( !(TString(systemFile->GetName()).Contains("0t20")) ) continue;

	FhdPhidEtaDistribution( systemFile, pi0GlobalInfo, "C_GET_RAW_REAL" );
	FhdPhidEtaDistribution( systemFile, pi0GlobalInfo, "C_GET_RAW_MIXED" );

	FhdPhidEtaDistributionCorrected( systemFile, pi0GlobalInfo, "");

	FhdPhidEtaDistributionProjEta( systemFile, pi0GlobalInfo, "C_GET_RAW_REAL" );
	FhdPhidEtaDistributionProjEta( systemFile, pi0GlobalInfo, "C_GET_RAW_MIXED" );

	FhdPhidEtaDistributionCorrectedProjEta( systemFile, pi0GlobalInfo, ""); 
}
