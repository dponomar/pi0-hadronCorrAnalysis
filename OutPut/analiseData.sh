#!/bin/bash
# $1 - run's collection name ( ex. 169589AOD )
red='\033[0;31m'
NC='\033[0m' # No Color

# Минимальный и максимальный импульс триггерной частицы
# (основывается на результатах спекстра масс в StartAnalise2Mass() и задаётся вручную.)
Pi0Min='6'
Pi0Max='16'

echo "$(tput setaf 2)Пределы по импульсам триггерных частиц: "
echo "           Pi0Min - $Pi0Min"
echo "           Pi0Max - $Pi0Max"
echo "Info: Пределы всегда можно поменять в analiseData.sh основываясь на резултатах StartAnalise2Mass()"
echo "$(tput sgr0)"

# Start
echo "$(tput setaf 2)================================================================================================================$(tput sgr0)";
 echo "$1 $2"
 echo "$(tput setaf 1)StartAnalise8Yield_IPA.C ( $1, $2, $Pi0Min, $Pi0Max ) $(tput sgr0)"; 		root -l -q -b StartAnalise8Yield_IPA.C\(\"$1\"\,\"$2\"\,\"$Pi0Min\"\,\"$Pi0Max\"\)

echo "$(tput setaf 2)================================================================================================================$(tput sgr0)";
