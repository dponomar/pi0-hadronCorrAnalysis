#!/bin/bash
# $1 - run coll name ( ex. 169589AOD )
echo ==== Обрабатываем пакет: $1
rm -r Hists/$1
mkdir Hists/$1

# Work with local GRID train (at ASUS ONLY!)
choise1="test"
choise2="train_pPb"
choise3="train_PbPb"

if [ $1 == "$choise1" ] ; then
		echo "You have chosen local data."
		cp /home/dponomar/Yandex.Disk/Master/NIRS/AdventureTime/AnalysisResults.root Hists/$1/AnalysisResults.root
	else
	if [ $1 == "$choise2" ] ; then
		echo "You have chosen local train pPb data."
		cp /home/dponomar/Yandex.Disk/Master/NIRS/Scripts/LegoTrains/LocalTrain_pPb/AnalysisResults.root Hists/$1/AnalysisResults.root
	else
		if [ $1 == "$choise3" ] ; then
			echo "You have chosen local train PbPb data."
			cp /home/dponomar/Yandex.Disk/Master/NIRS/Scripts/LegoTrains/LocalTrain_PbPb/AnalysisResults.root Hists/$1/AnalysisResults.root
		else
			# Work whith  GRID/Arhive
			echo "You have chosen GRID/Arhive data."
			cp Archive/InProgress/$1.root Hists/$1/AnalysisResults.root
		fi
	fi
fi


cp cphisto2.C Hists/$1
# Making them more pretty
cd Hists/$1/
root -l -q -b cphisto2.C\(\"$1\"\)
rm cphisto2.C AnalysisResults.root
cd -

if [ $1 == "PbPb11h" ] ; then
	echo "Merging different centralities: 0-10% + 10-20% = 0-20%"
	. mergeCent.sh $1
fi

echo "Merging left and right sides"
. mergeLR.sh $1


echo  Something Done! ================