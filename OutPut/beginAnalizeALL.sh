#!/bin/bash
startALL=$SECONDS # Начало выполнения скрипта в секундах.
echo "$(tput setaf 2)Это на долго. Можешь пойти выпить кофе с печеньками.$(tput sgr0)"
echo "$(tput setaf 2)Только пожалуйста УБЕДСЬ что пределы импульсов триггерных частиц в analise.sh и analiseData.sh выставлены ПРАВИЛЬНО!$(tput sgr0)"
mkdir log
rm log/* # Чистим все логи от прошлых запусков.

# Рабочие датасеты для анализа.
folder1='New/PbPb11h'
folder2='New/pPb13'
folder3='New/PbPb_PCMgood'
folder4='PbPb_RCTAndJPsigood'

 echo "analise.sh $folder1"; . analise.sh $folder1 >& log/ana_${folder1}.log

 # echo "analise.sh $folder2"; . analise.sh $folder2 #>& log/ana_${folder2}.log

 # echo "analise.sh $folder3"; . analise.sh $folder3 #>& log/ana_${folder3}.log

 # echo "analise.sh $folder4"; . analise.sh $folder4 #>& log/ana_${folder4}.log

# Работа с периодами 13 года.
# f=$folder1"b";echo "analise.sh $f"; . analise.sh "$f" >& log/ana_${f}.log
# f=$folder1"c";echo "analise.sh $f"; . analise.sh "$f" >& log/ana_${f}.log
# f=$folder1"d";echo "analise.sh $f"; . analise.sh "$f" >& log/ana_${f}.log
# f=$folder1"e";echo "analise.sh $f"; . analise.sh "$f" >& log/ana_${f}.log
# f=$folder1"f";echo "analise.sh $f"; . analise.sh "$f" >& log/ana_${f}.log


 # echo "analiseData.sh $folder1 $folder2 "; . analiseData.sh $folder1 $folder2 >& log/ana_${folder1}_${folder2}.log
 # echo "analiseData.sh $folder1 $folder2 "; . analiseData.sh $folder1 $folder2 >& log/ana_${folder1}_${folder2}.log
# echo "analiseData.sh $folder3 $folder3 "; . analiseData.sh $folder3 $folder3 >& log/ana_${folder3}_${folder3}.log
# echo "analiseData.sh $folder4 $folder4 "; . analiseData.sh $folder4 $folder4 >& log/ana_${folder4}_${folder4}.log

# echo "analiseData.sh $folder2 $folder1 "; . analiseData.sh $folder2 $folder1 >& log/ana_${folder2}_${folder1}.log
# echo "analiseData.sh $folder2 $folder4 "; . analiseData.sh $folder2 $folder4 >& log/ana_${folder2}_${folder4}.log


echo "$(tput setaf 2)Готово! Глобальный анализ завершён.$(tput sgr0)";
# Конец выполнения скрипта. Считаем потраченное время и выводим на экран.
durationALL=$(( SECONDS - startALL ))
echo "$(tput setaf 6)Времени потрачено: $durationALL (sec)$(tput sgr0)"