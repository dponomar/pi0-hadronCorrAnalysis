#!/bin/bash
# Merge root files with different sides from nass peak.
# $1 - run's collection name ( ex. 169589AOD )

root -l -b -q MergeResultsLR.C\(\"$1\"\)