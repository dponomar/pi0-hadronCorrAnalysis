#!/bin/bash
# Merge root files with different centralities.
# $1 - run's collection name ( ex. 169589AOD )

root -l -b -q MergeResultsByCentrality.C\(\"$1\"\,\"0\"\,\"10\"\,\"20\"\)