#!/bin/bash
# $1 - run's collection name ( ex. 169589AOD )
startA=$SECONDS # Начало выполнения скрипта в секундах.

# Массивы по импульсам триггерных частиц
declare -a arrayPtmin
declare -a arrayPtmax
arrayPtmin=( [1]=2 [2]=3  [3]=4  [4]=5  [5]=6  [6]=7  )
arrayPtmax=( [1]=4 [2]=12 [3]=14 [4]=16 [5]=16 [6]=16 )


for i in 1 4 6
# for i in 4 
#for i in 5
do
	interval=$SECONDS

	# Запускаем обработку
	echo "$(tput setaf 2)$i) Folder: $1 pTmin: ${arrayPtmin[$i]} Ptmax: ${arrayPtmax[$i]}$(tput sgr0)"
	. main.sh $1 ${arrayPtmin[$i]} ${arrayPtmax[$i]} #>& log/main_T${arrayPtmin[$i]}t${arrayPtmax[$i]}.log

	# Считаем время
	interval=$(( SECONDS - interval ))
	Chas=$(( $interval/3600 ))
	Min=$(( $(( $interval-$Chas*3600 ))/60 ))
	Sek=$(( $interval-$(( $Chas*3600+$Min*60 )) ))
	echo "	Интервал: $Chas:$Min:$Sek"

	if [ $i -ne 6 ]
	then
		toTheEnd=$[$[4-$i]*$interval]
		Chas=$(( $toTheEnd/3600 ))
		Min=$(( $(( $toTheEnd-$Chas*3600 ))/60 ))
		Sek=$(( $toTheEnd-$(( $Chas*3600+$Min*60 )) ))
		echo "	До окончания примерно $Chas:$Min:$Sek"
	fi
done

# Конец выполнения скрипта. Считаем полное потраченное время и выводим на экран.
duration=$(( SECONDS - startA ))
Chas=$(( $duration/3600 ))
Min=$(( $(( $duration-$Chas*3600 ))/60 ))
Sek=$(( $duration-$(( $Chas*3600+$Min*60 )) ))

echo "$(tput setaf 6)"
echo "Анализ скриптом analise.sh ($1) завершён."
echo "Времени потрачено в analise.sh  $Chas:$Min:$Sek"
echo "$(tput sgr0)"

# Notification in KDE
notify-send "analise.sh" "Анализ скриптом analise.sh ($1) завершён."