#include <Scripts/Angle/2D/FhdPhidEtaDistributionCorrected.C>

FhdPhidEtaDoubleRidge (	const TSystemFile* systemFile_1, const TSystemFile* systemFile_2, const Pi0GlobalInfo pi0GlobalInfo, const TString flag )
{
	/*	Попытка увидеть добл ридж эффект в 2D распределениях для pPb столкновений.
		Деалает это для различных pT ассоциированной частицы и различных pid триггерной частицы.
	*
	*	systemFile_1 	- указатель на файл ()
	*	systemFile_2 	- указатель на файл ()
	*	pi0GlobalInfo 	- информация о интервале pT тригерного pi0 для анализа. Без селекции по pid!
	* 	flag 			- управление различными сценариями:
			"C_USE_MB_DATA"			- использует азимутальные корреляционные распределения набранные 
									  с триггером предназначенным для набора смешанных событий. По умолчанию берутся события с триггером для реальных распределений.
	*/
	PrintInTerminal_Yellow(Form("FhdPhidEtaDoubleRidge with flag %s", flag.Data()), flag);

	Pi0Info pi0Info;
	pi0Info.ptMin = pi0GlobalInfo.ptMin;
	pi0Info.ptMax = pi0GlobalInfo.ptMax;

	TArrayD pTassoc = GetPtBining();
	Pi0IPDType type = GetPi0IPDType();

	// int i = 3;
	// int j = 3;
	for (int i = 0; i < type.nType; ++i)
	{
		pi0Info.cutType = type.type[i];
		for (int j = 0; j < pTassoc.GetSize() && pTassoc.At(j)< pi0GlobalInfo.ptMin; ++j)
			FhdPhidEtaDoubleRidge_Work ( systemFile_1, systemFile_2, pi0Info, flag, pTassoc[j], pTassoc[j+1] );
	}
	PrintInTerminal_Green("");

	FhdPhidEtaDoubleRidge_Work ( systemFile_1, systemFile_2, pi0Info, flag, pTassoc[0], pTassoc[5] );
}



FhdPhidEtaDoubleRidge_Work ( const TSystemFile* systemFile_1, const TSystemFile* systemFile_2, const Pi0Info pi0Info, const TString flag, 
																										Double_t pTAssocMin, Double_t pTAssocMax )
{	
	/*	Попытка увидеть добл ридж эффект в 2D распределениях для pPb столкновений.
	*
	*	systemFile_1 	- указатель на файл ()
	*	systemFile_2 	- указатель на файл ()
	*	pi0Info 	    - информация о интервале pT и pid тригерного pi0 для анализа
	* 	flag 			- управление различными сценариями:			
			<Empty>

		FhdPhiDistribution_Work():	
			"C_USE_MB_DATA"			- использует азимутальные корреляционные распределения набранные 
									  с триггером предназначенным для набора смешанных событий. По умолчанию берутся события с триггером для реальных распределений.

	*	pTAssocMin		- минимальный pT ассоциированных частиц	
	*	pTAssocMax		- максимальный pT ассоциированных частиц
	*
	*	Сохраняет картинки в ../Comparison/EffeciencyCorrection/Angle*pidCut* или ../Comparison/EffeciencyCorrection/Angle/MB/*pidCut* при включённом "C_USE_MB_DATA"
	*/

	const TFile* f1 = OpenFile(systemFile_1, flag);	// Тут хитрая работа с файлами, на основе cd().
	const TFile* f2 = OpenFile(systemFile_2, flag);
	// Ещё раз на всякий случай переносимся в рабочую папку.
 	gSystem->cd( systemFile_1->GetTitle() );
	PrintInTerminal_Yellow(Form("FhdPhidEtaDoubleRidge_Work for ( %s ) - ( %s ) with flag \"%s\" for pT from %2.1f to %2.1f ", systemFile_1->GetName(), systemFile_2->GetName(), flag.Data(), pi0Info.ptMin, pi0Info.ptMax ), flag);
	if(flag.Contains("F_USE_CURRENT_FILE") || flag.Contains("GET_")) { PrintInTerminal_Blue("Check your flag config. Flag \"F_USE_CURRENT_FILE\" or \"GET_\" impossible for FhdPhidEtaDoubleRidge_Work.", flag); exit(1);}

	// Заполняем данные для шапок
	TString dataSet, centrality1, centrality2;
	f1->cd();
	dataSet = MakeCanvasDatasetLabel();
	centrality1 = MakeCanvasCentralityLabel(f1->GetName());
	f2->cd();
	centrality2 = MakeCanvasCentralityLabel(f2->GetName());

	TString histName = "DoubleRidge";
	gStyle->SetOptFit(1);
	gStyle->SetOptStat(0);

	//----------------correlations
	f1->cd();
	TH2D *hphi_1 = FhdPhidEtaDistributionCorrected_Work( systemFile_1, pi0Info, flag+" F_USE_CURRENT_FILE D_GET_FAST", pTAssocMin, pTAssocMax );
	f2->cd();
	TH2D *hphi_2 = FhdPhidEtaDistributionCorrected_Work( systemFile_2, pi0Info, flag+" F_USE_CURRENT_FILE D_GET_FAST", pTAssocMin, pTAssocMax );

	// Вычитаем f2 из f1.
	hphi_1-> Add(hphi_2, -1);

	TH2D* hPhiOutput = hphi_1->Clone();
	//hPhiOutput->RebinX(2);

	// Рисуем =======================================================
	TCanvas * canvas = new TCanvas( (histName).Data(), histName.Data(),1100,800);

	hPhiOutput->SetMarkerStyle(20);
	hPhiOutput->SetMarkerColor(4);
	hPhiOutput->GetYaxis()->SetRangeUser(-0.7, 0.7);
	hPhiOutput->SetTitle("Double ridge");
	hPhiOutput->GetXaxis()->SetTitleOffset(1.45);
	hPhiOutput->GetYaxis()->SetTitleOffset(1.45);
	hPhiOutput->GetZaxis()->SetTitleOffset(1.45);
	hPhiOutput->GetZaxis()->SetTitle("(1/N_{trig})dN/d#Delta#phid#Delta#eta");       
	hPhiOutput->GetXaxis()->SetTitle("#Delta#phi [rad]");
	hPhiOutput->GetYaxis()->SetTitle("#Delta#eta");                                                    
	gPad->SetTheta(25);
	gPad->SetPhi(35);

	hPhiOutput->Draw("surf1");                             
	
	// TString text1 = "[0-10\%]-";
	// TString text2 = "[20-50\%]";
	TString text1 = Form("%s_{%s} -", dataSet.Data(), MakeCanvasCentralityLabel(f1->GetName()).Data() );
	TString text2 = Form("%s_{%s}", dataSet.Data(), MakeCanvasCentralityLabel(f2->GetName()).Data() );
	Double_t pos[2] = {0.65, 0.92};
	MakeLabelForPublicationCentralityDependense( pos, true, 0.04, text1, text2, dataSet.Data(), pi0Info.ptMin, pi0Info.ptMax, pTAssocMin, pTAssocMax );


	TString stTargetFile1 = f1->GetName();
	TString stTargetFile2 = f2->GetName();
	CutRootPart(stTargetFile1);
	CutRootPart(stTargetFile2);
	CutMergePartFromDataSet(stTargetFile1);
	CutMergePartFromDataSet(stTargetFile2);

	TString folderNameForSave = Form("[%s]-[%s]", MakeCanvasCentralityLabel(f1->GetName()).Data(), MakeCanvasCentralityLabel(f2->GetName()).Data() ) ;

	// Формируем имя пипки
	gSystem->cd( systemFile_1->GetTitle() );
	TString folder =systemFile_1->GetTitle();
	ChangeDirectory(folder.Data());

	folder = folder+"/UnionComparasion";
	ChangeDirectory(folder.Data());

	folder = folder+"/DoubleRidge";
	ChangeDirectory(folder.Data());

	folder = folder+"/"+folderNameForSave;
	ChangeDirectory(folder.Data());

	if(flag.Contains("C_USE_MB_DATA"))
	{
		folder = folder+"/MB";
		ChangeDirectory(folder.Data());
	}

	folder = folder+"/"+TString(pi0Info.cutType);
	ChangeDirectory(folder.Data());

	// Формируем имя файла.
	TString ptassmints = (toString(double(pTAssocMin))).c_str();
	TString ptassmaxts = (toString(double(pTAssocMax))).c_str();
	TString pi0minst = (toString(double(pi0Info.ptMin))).c_str();
	TString pi0maxst = (toString(double(pi0Info.ptMax))).c_str();
	TString pictureName = "A"+replesePoint( ptassmints.Data() )	+ "t"+replesePoint( ptassmaxts.Data() )
						+ "T"+replesePoint( pi0minst.Data() )	+ "t"+replesePoint( pi0maxst.Data() ) 
						+ ".png";
   	canvas->SaveAs(pictureName.Data());

	canvas->Close();
	// Возвращаемся в исходную папку.
 	gSystem->cd( systemFile_1->GetTitle() );		
	f1->Close();
	f2->Close();
	PrintInTerminal_Green("");
	// PrintInTerminal_Green("Done FhdPhidEtaDoubleRidge_Work");
}