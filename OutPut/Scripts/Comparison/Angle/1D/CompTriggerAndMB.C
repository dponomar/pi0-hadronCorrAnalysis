#include <Scripts/Angle/1D/FhdPhiDistribution.C>

CompTriggerAndMB( const TSystemFile* systemFile, const Pi0GlobalInfo pi0GlobalInfo, const TString flag )
{
	/*	Сравнивает 1D гистограммы набранные с использованием различных триггеров на события (только MB+PHOS и MB).
		Деалает это для различных pT ассоциированной частицы и различных pid триггерной частицы.
	*
	*	systemFile 	- указатель на файл ()
	*	pi0GlobalInfo 	- информация о интервале pT тригерного pi0 для анализа. Без селекции по pid!
	* 	flag 			- управление различными сценариями:
			"C_USE_MB_DATA"			- использует азимутальные корреляционные распределения набранные 
									  с триггером предназначенным для набора смешанных событий. По умолчанию берутся события с триггером для реальных распределений.
	*/
	PrintInTerminal_Yellow(Form("CompTriggerAndMB with flag %s", flag.Data()));

	Pi0Info pi0Info;
	pi0Info.ptMin = pi0GlobalInfo.ptMin;
	pi0Info.ptMax = pi0GlobalInfo.ptMax;

	TArrayD pTassoc = GetPtBining();
	Pi0IPDType type = GetPi0IPDType();

	// int i = 3;
	// int j = 3;
	for (int i = 0; i < type.nType; ++i)
	{
		pi0Info.cutType = type.type[i];
		for (int j = 0; j < pTassoc.GetSize() && pTassoc.At(j)< pi0GlobalInfo.ptMin; ++j)
			CompTriggerAndMB_Work ( systemFile, pi0Info, flag, pTassoc[j], pTassoc[j+1] );
	}
	PrintInTerminal_Green("");
}

CompTriggerAndMB_Work (	const TSystemFile* systemFile, const Pi0Info pi0Info, const TString flag, Double_t pTAssocMin, Double_t pTAssocMax )
{	
	/*	Сравнивает 1D гистограммы набранные с использованием различных триггеров на события (только MB+PHOS и MB).
	*
	*	systemFile	 	- указатель на файл ()
	*	pi0Info 	    - информация о интервале pT и pid тригерного pi0 для анализа
	* 	flag 			- управление различными сценариями:			
			<Empty>
	*	pTAssocMin		- минимальный pT ассоциированных частиц	
	*	pTAssocMax		- максимальный pT ассоциированных частиц
	*
	*	Сохраняет картинки в ../Comparison/EventTriggerSelection/Angle/*pidCut*
	* 	Закрывает за собой файл.
	*/

	const TFile* f = OpenFile(systemFile, flag);
	// Ещё раз на всякий случай переносимся в рабочую папку.
 	gSystem->cd( systemFile->GetTitle() );
	PrintInTerminal_Yellow(Form("CompTriggerAndMB_Work with flag \"%s\" for pT_trigg %2.1f --> %2.1f GeV/c for cut \"%s\" and pT_assoc %2.1f --> %2.1f GeV/c",
								 flag.Data(), pi0Info.ptMin, pi0Info.ptMax, pi0Info.cutType.Data(), pTAssocMin, pTAssocMax), flag);
	
	string histName = "ETS";
	gStyle->SetOptFit(1);
	gStyle->SetOptStat(0);


	TH1D* hTrigger 	= FhdPhiDistribution_Work ( systemFile, pi0Info, flag+"  F_USE_CURRENT_FILE D_GET_CORRECTED_FAST", pTAssocMin, pTAssocMax ); 			// All
	TH1D* hMB 		= FhdPhiDistribution_Work ( systemFile, pi0Info, flag+"  F_USE_CURRENT_FILE D_GET_CORRECTED_FAST C_USE_MB_DATA", pTAssocMin, pTAssocMax ); // MB only

	if(!hMB) { PrintInTerminal_Red("No MB hist to compare", flag); return; }

	// Рисуем =======================================================
	TCanvas * canvas = new TCanvas( (histName).c_str(), histName.c_str(),900,600);

	// hTrigger->Rebin(2); // по eta сливает по n бина
	// hMB->Rebin(2); // по eta сливает по n бина

	hTrigger->SetMarkerStyle(20);
	hTrigger->SetMarkerColor(4);
	hMB->SetMarkerStyle(20);
	hMB->SetMarkerColor(2);
	hTrigger->SetTitle(" ");
	hTrigger->GetYaxis()->SetTitle("(1/N_{trig})dN_{ass}/d#Delta#phi (rad^{-1})");
	hTrigger->GetXaxis()->SetTitle("#Delta#phi [rad]");
	hTrigger->Draw("");
	hTrigger->GetYaxis()->SetRangeUser(0., hTrigger->GetMaximum()+0.05*hTrigger->GetMaximum() );
	hMB->SetLineColor(2);
	hMB->Draw("same");

	TLine *line = new TLine(-TMath::Pi()/2.,0,TMath::Pi()*3./2.,0);
 	line->SetLineWidth(2);
 	line->SetLineStyle(7);
 	line->Draw("same");

 	Double_t pos1[2] = {0.65, 0.85};
 	MakeStandartLabelForPublication(pos1, true , 0.04, f->GetName(), MakeCanvasDatasetLabel(), pi0Info.ptMin, pi0Info.ptMax, pTAssocMin, pTAssocMax);

 	// Double_t pos2[2] = {0.6, 0.55};
	// MakeSimplLabel( pos2, "ALICE work in progress" );

	TLegend *leg1 = new TLegend(0.65,0.5,0.85,0.59);
		// leg1->SetFillStyle(0);
		leg1->SetFillColor(0);
		leg1->SetShadowColor(1);
		leg1->AddEntry( hTrigger,"kPHI7+kINT7 events","lp");
		leg1->AddEntry( hMB,"kINT7 events only","lp");
		leg1->Draw("same");

	// Формируем имя пипки
	gSystem->cd( systemFile->GetTitle() );
	TString folder =systemFile->GetTitle();

	TString stTargetFile = gFile->CurrentFile()->GetName();
	CutRootPart(stTargetFile);
	CutMergePartFromDataSet(stTargetFile);
	folder = folder+stTargetFile;
	ChangeDirectory(folder.Data());

	folder = folder+"/Comparison";
	ChangeDirectory(folder.Data());
	folder = folder+"/EventTriggerSelection";
	ChangeDirectory(folder.Data());
	folder = folder+"/Angle";
	ChangeDirectory(folder.Data());
	folder = folder+"/"+TString(pi0Info.cutType);
	ChangeDirectory(folder.Data());	

	// Формируем имя файла.
	TString ptassmints = (toString(double(pTAssocMin))).c_str();
	TString ptassmaxts = (toString(double(pTAssocMax))).c_str();
	TString pi0minst = (toString(double(pi0Info.ptMin))).c_str();
	TString pi0maxst = (toString(double(pi0Info.ptMax))).c_str();
	TString pictureName = "A"+replesePoint( ptassmints.Data() )	+"t"+replesePoint( ptassmaxts.Data() )
						+ "T"+replesePoint( pi0minst.Data() )	+"t"+replesePoint( pi0maxst.Data() ) 
						+ ".png";
	canvas->SaveAs(pictureName.Data());

	canvas->Close();

	// Возвращаемся в исходную папку.
 	gSystem->cd( systemFile->GetTitle() );

	f->Close();
}