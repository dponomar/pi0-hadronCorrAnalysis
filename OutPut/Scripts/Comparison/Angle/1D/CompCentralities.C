#include <Scripts/Angle/1D/FhdPhiDistribution.C>

CompCentralities( const TObjArray* fileList, const Pi0GlobalInfo pi0GlobalInfo, const TString flag )
{
	/*	Сравнивает 1D гистограммы с разными центральностями на одном графике.
		Деалает это для различных pT ассоциированной частицы и различных pid триггерной частицы.
	*
	*	filelist 	- список systemFile файлов
	*	pi0GlobalInfo 	- информация о интервале pT тригерного pi0 для анализа. Без селекции по pid!
	* 	flag 			- управление различными сценариями:
			"C_USE_MB_DATA"			- использует азимутальные корреляционные распределения набранные 
									  с триггером предназначенным для набора смешанных событий. По умолчанию берутся события с триггером для реальных распределений.
	*/
	PrintInTerminal_Yellow(Form("CompCentralities() with flag %s", flag.Data()), flag);

	Pi0Info pi0Info;
	pi0Info.ptMin = pi0GlobalInfo.ptMin;
	pi0Info.ptMax = pi0GlobalInfo.ptMax;

	TArrayD pTassoc = GetPtBining();
	Pi0IPDType type = GetPi0IPDType();

	// int i = 3;
	// int j = 3;
	for (int i = 0; i < type.nType; ++i)
	{
		pi0Info.cutType = type.type[i];
		for (int j = 0; j < pTassoc.GetSize() && pTassoc.At(j)< pi0GlobalInfo.ptMin; ++j)
			CompCentralities_Work ( fileList, pi0Info, flag, pTassoc.At(j), pTassoc.At(j+1) );
	}
}

CompCentralities_Work ( const TObjArray* fileListIncome, const Pi0Info pi0Info, const TString flag, Double_t pTAssocMin, Double_t pTAssocMax )
{	
	/*	Сравнивает 1D гистограммы с разными центральностями на одном графике. Перебирает весь входящий список файлов и рисует Default графики.
	*
	*	systemFile	 	- указатель на файл ()
	*	pi0Info 	    - информация о интервале pT и pid тригерного pi0 для анализа
	* 	flag 			- управление различными сценариями:			
			<Empty>
		FhdPhiDistribution_Work():
			"C_USE_MB_DATA"			- использует азимутальные корреляционные распределения набранные 
									  с триггером предназначенным для набора смешанных событий. По умолчанию берутся события с триггером для реальных распределений.
	*	pTAssocMin		- минимальный pT ассоциированных частиц	
	*	pTAssocMax		- максимальный pT ассоциированных частиц
	*
	*	Сохраняет картинки в ../UnionComparasion/Centrality/Angle/*pidCut*
	*/

	// Сортируем по возрастающей центральности.
	TObjArray* fileList = SortFileListByCentrality_Default(fileListIncome);

	// Общая информация
	TString dataSet = MakeCanvasDatasetLabel();
	// Массив под гистограммы которые достали из файлов.
	TObjArray* histList = new TObjArray();

	Int_t histCount = 0;
	
	// Создаём канвас куда будем рисовать.
	TCanvas * canvas = new TCanvas( "hCentDiff", "CentDiff",1000, 800);
	TLegend *leg = new TLegend(0.85,0.7,0.99,0.93);
	leg->SetFillColor(0);
	leg->SetShadowColor(1);

	TH1D* h = 0x0;

	// Цикл по всем файлам центральностей.
	for (int i = 0; i < fileList->GetEntries(); ++i)
	{
		// Тянем файл и из него гистограмму.
		TSystemFile* systemFile = (TSystemFile*)fileList->At(i);
		if( !(TString(systemFile->GetName()).Contains("Default")) ) continue;
		if (!TestRootFilieQuality(systemFile)) continue;

		TFile* f = OpenFile(systemFile, flag);
 		gSystem->cd( systemFile->GetTitle() ); // Ещё раз на всякий случай переносимся в рабочую папку.
 		TH1D* h  = FhdPhiDistribution_Work( systemFile, pi0Info, flag+" F_USE_CURRENT_FILE D_GET_CORRECTED_FAST", pTAssocMin, pTAssocMax ); 
 		
 		TH1D* hBuff = (TH1D*)h->Clone(Form("%s", systemFile->GetName()));
 		// Пишем её клон в наш буферный массив
 		hBuff->SetTitle(systemFile->GetName());
 		histList->Add(hBuff);

		// Вычитаем подложку по простому B0
		Double_t b0 = GetB0(hBuff, "");
		TF1* fit = new TF1("func","[0]", -1*TMath::Pi()/2., 1.5*TMath::Pi());
		fit->FixParameter(0, b0);
		hBuff->Add(fit, -1.);

		// Пересчитываем на единицу псевдобыстроты
		hBuff->Scale(1./0.9);

		h = hBuff;
		// Рисуем
		h->SetMarkerStyle(GetNiceMarkerNumber(histCount));
		h->SetMarkerSize(1.1);
		h->SetMarkerColor(GetNiceColorNumber(histCount));
		h->SetLineColor(GetNiceColorNumber(histCount));
		h->GetYaxis()->SetTitleSize(0.04);
		h->GetYaxis()->SetTitleOffset(1.1);
		h->GetYaxis()->SetTitle("1/N_{trig} dN_{assoc}/d#Delta#phi per #Delta#eta - const (rad^{-1})");
		h->GetXaxis()->SetTitle("#Delta#phi [rad]");

		if( TString(systemFile->GetTitle()).Contains("pPb") )
			if(pTAssocMin ==1 && pTAssocMax == 2)
				h->GetYaxis()->SetRangeUser(-0.03, 0.2);
			else
				h->GetYaxis()->SetRangeUser(-0.03, 0.3);
		else
			h->GetYaxis()->SetRangeUser(-0.03, 0.6);

		//cout<<"==="<<h->GetTitle()<<"\n"<<endl;
		leg->AddEntry(h, Form("%s",MakeCanvasCentralityLabel(h->GetTitle()).Data()),"lp");
		h->SetTitle(" ");

		if(histCount==0) h->Draw("");
		else h->Draw("same");

		histCount++;

		//f->Close();
	}


	leg->Draw("same");

	TLine *line = new TLine(-TMath::Pi()/2.,0,TMath::Pi()*3./2.,0);
	 	line->SetLineWidth(2);
	 	line->SetLineStyle(7);
	 	line->Draw("same");

	Double_t pos1[2] = {0.55, 0.85};
 	MakeStandartLabelForPublication_PIDinseadCentrality(pos1, true , 0.03, f->GetName(), MakeCanvasDatasetLabel(), pi0Info.cutType, pi0Info.ptMin, pi0Info.ptMax, pTAssocMin, pTAssocMax);
 	
 	Double_t pos2[2] = {0.65, 0.65};
	MakeSimplLabel( pos2, "ALICE work in progress" );

	// Формируем имя пипки
	gSystem->cd( systemFile->GetTitle() );
	TString folder =systemFile->GetTitle();

	// TString stTargetFile = gFile->CurrentFile()->GetName();
	// CutRootPart(stTargetFile);
	// CutMergePartFromDataSet(stTargetFile);
	// folder = folder;
	ChangeDirectory(folder.Data());

	folder = folder+"/UnionComparasion";
	ChangeDirectory(folder.Data());
	folder = folder+"/Centrality";
	ChangeDirectory(folder.Data());
	folder = folder+"/Angle";
	ChangeDirectory(folder.Data());

	if(flag.Contains("C_USE_MB_DATA"))
	{
		folder = folder+"/MB";
		ChangeDirectory(folder.Data());
	}

	folder = folder+"/"+TString(pi0Info.cutType);
	ChangeDirectory(folder.Data());

	// Формируем имя файла.
	TString ptassmints = (toString(double(pTAssocMin))).c_str();
	TString ptassmaxts = (toString(double(pTAssocMax))).c_str();
	TString pi0minst = (toString(double(pi0Info.ptMin))).c_str();
	TString pi0maxst = (toString(double(pi0Info.ptMax))).c_str();
	TString pictureName = "A"+replesePoint( ptassmints.Data() )	+"t"+replesePoint( ptassmaxts.Data() )
						+ "T"+replesePoint( pi0minst.Data() )	+"t"+replesePoint( pi0maxst.Data() ) 
						+ ".png";
	canvas->SaveAs(pictureName.Data());
	canvas->Close();

	f->Close();
}
