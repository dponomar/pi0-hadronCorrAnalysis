#include "TFile.h"

TF1 * FhMassInvVsPtTriger (	const TSystemFile* systemFile, const Pi0GlobalInfo Pi0GlobalInfo, const TString flag, int binMult, int binWidth, int downPt )
{
	/*	Рисует распределения положения пика и его ширины в зависимости от pT интервала Pi0GlobalInfo. Фитирует both распределения.
		Пишет этот результат параметризации в ../Mass/Fit/data.txt
	*	Возвращает по умолчанию нулевой указатель. Может возвращать фиты массы или сигмы.
	*
	*	systemFile 		- указатель на файл
	*	Pi0GlobalInfo 	- информация о интервале pT тригерного pi0 для анализа
	* 	flag 			- управление различными сценариями:
			"F_USE_CURRENT_FILE"	- не закрывает текущий файл.
			
			"D_DEBUG_PICTURES"	    - рисует в папке Mass/StepByStep/*pidCut* пошаговое вычисление сигнала и отношения сигнал/шум.

			"C_GET_FIT_MASS"        - возвращает фит зависимости положения пика массы от pT
			"C_GET_FIT_SIGMA" 		- возвращает фит зависимости ширины пика массы от pT

	*	binMult			- число бинов 
	* 	binWidth		- ширина каждого бина в GeV/c
	* 	downPt 			- стартовое значение бина
	*/

	const TFile* f = OpenFile(systemFile, flag);

	// Ещё раз на всякий случай переносимся в рабочую папку.
 	gSystem->cd( systemFile->GetTitle() );
	PrintInTerminal_Yellow(Form("FhMassInvVsPtTriger with flag %s for pT from %i to %i", flag.Data(), downPt, downPt+binMult*binWidth), flag);

	Pi0IPDType pidType = GetPi0IPDType();

	Double_t Mass [4][30], MassError [4][30];
	Double_t Sigma[4][30], SigmaError[4][30];
	Double_t pT[30], pTError[30];

	Pi0Info pi0Info;

	for(int pid=0; pid <4; pid++)
	{
		for (int ptBin = 0; ptBin < binMult; ptBin++) 
		{
			pi0Info.ptMin = ptBin*binWidth+downPt;
			pi0Info.ptMax = (ptBin+1)*binWidth+downPt;
			pi0Info.cutType = pidType.type[pid];

			InfoTransfer transfer = FhMass_RealMix_Work( systemFile, pi0Info, flag+" F_USE_CURRENT_FILE C_GET_MASS_FIT_DATA" );

			Mass [pid][ptBin] = fabs( transfer.mean );
			Sigma[pid][ptBin] = fabs( transfer.intreal );
			MassError [pid][ptBin] = fabs( transfer.error );
			SigmaError[pid][ptBin] = fabs( transfer.intmix );
			pT[ptBin] = (double((ptBin+1)*binWidth+downPt)-double(ptBin*binWidth+downPt))/2. + double(ptBin*binWidth+downPt) ;
			pTError[ptBin] = (double((ptBin+1)*binWidth+downPt)-double(ptBin*binWidth+downPt))/2. ;
		}
	}
	
	// 1 ============================
	TCanvas * canvasMass = new TCanvas( "MassPt", "MassPt", 800, 800 );
	canvasMass->SetGrid();
	gStyle->SetOptFit(1);

	Int_t lineWidth = 2;

	TGraphErrors* grh[4];
	// int i = 0;
	for (int i = 0; i < pidType.nType; ++i)
	{
		grh[i] = new TGraphErrors(binMult, pT, Mass[i], pTError, MassError[i]);
		//grh[i]->SetFillStyle(0);
		grh[i]->SetMarkerStyle(21);
		grh[i]->SetMarkerColor(pidType.PIDColor[i]);
		grh[i]->SetLineColor(pidType.PIDColor[i]);
		grh[i]->SetLineWidth(lineWidth);
		grh[i]->SetFillColor(pidType.PIDColor[i]);
		grh[i]->SetTitle("M_{#pi_{0}}"); //название графика
		grh[i]->GetYaxis()->SetTitle("M_{#pi_{0}}, GeV"); //название осей
		grh[i]->GetYaxis()->SetTitleOffset(1.37);
		grh[i]->GetXaxis()->SetTitle("pT_{#gamma#gamma}, GeV/c"); 
		grh[i]->GetXaxis()->SetRangeUser(downPt-0.5, downPt+binMult*binWidth +0.5);

		if(TString(systemFile->GetTitle()).Contains("PbPb"))
			grh[i]->GetYaxis()->SetRangeUser(0.133,0.143);
		else
			grh[i]->GetYaxis()->SetRangeUser(0.131,0.137);

		if(i==0) grh[i]->Draw("AEP");
		else grh[i]->Draw("sameE");
	}
	
	for (int i = 0; i < pidType.nType; ++i)
		grh[i]->Draw("samePX");

	Double_t pos[2] = {0.15, 0.86};
	MakeStandartLabelForPublicationMinimal( pos, true, f->GetName(), MakeCanvasDatasetLabel() );

	Double_t pos2[2] = {0.43, 0.15};
	MakeSimplLabel( pos2, "ALICE work in progress" );

	TLegend *leg1 = new TLegend(0.8,0.63,0.90,0.77);
	leg1->SetFillStyle(0);
	leg1->SetBorderSize(0);
	for (int i = 0; i < 4; ++i)
	{
		leg1->AddEntry(grh[i], pidType.canvasName[i],"lp");
	}
	leg1->Draw("same");

	TString stTargetFile = f->GetName();
	CutRootPart(stTargetFile);
	CutMergePartFromDataSet(stTargetFile);
	ChangeDirectory( string(systemFile->GetTitle()), "", string(stTargetFile.Data()) );
	string pictureNameMass = "MassInvVsPtTrig.png";
	canvasMass->SaveAs(pictureNameMass.c_str());
	// Возвращаемся в исходную папку.
	gSystem->cd( systemFile->GetTitle() );


	// Делаем фитирование массы по Both2Core
	TString sFPtMass = "[0]*x+[1]"; 
	TF1 *fit_pt_mass = new TF1("funcMassPt",sFPtMass.Data(), downPt, downPt+binMult*binWidth );
	fit_pt_mass->SetParameter(0, -0.0001);
	fit_pt_mass->SetParameter(1, 0.134);
	if(MakeCanvasDatasetLabel()).Contains("PbPb") )
	{
		//fit_pt_mass->SetParameter(0, -0.01);
		fit_pt_mass->SetParLimits(0, -0.01, 0.01);
		//fit_pt_mass->SetParameter(1, 0.14);
		fit_pt_mass->SetParLimits(1, 0.125, 0.155);
	}
	grh[3]->Fit(fit_pt_mass,"RQ");

	ChangeDirectory( string(systemFile->GetTitle()), "", string(stTargetFile.Data()), "Mass", "Fit" );
	canvasMass->SaveAs(pictureNameMass.c_str());
	canvasMass->Close();
	// Возвращаемся в исходную папку.
 	gSystem->cd( systemFile->GetTitle() );


 	// 2 ============================
 	TCanvas * canvasSigma = new TCanvas( "SigmaPt", "SigmaPt", 800, 800 );
	canvasSigma->SetGrid();
	gStyle->SetOptFit(1);

	// int i = 0;
	for (int i = 0; i < pidType.nType; ++i)
	{
		grh[i] = new TGraphErrors(binMult, pT, Sigma[i], pTError, SigmaError[i]);
		//grh[i]->SetFillStyle(0);
		grh[i]->SetMarkerStyle(21);
		grh[i]->SetMarkerColor(pidType.PIDColor[i]);
		grh[i]->SetLineColor(pidType.PIDColor[i]);
		grh[i]->SetLineWidth(lineWidth);
		grh[i]->SetFillColor(pidType.PIDColor[i]);
		grh[i]->SetTitle("#sigma_{#pi_{0}}"); //название графика
		grh[i]->GetYaxis()->SetTitle("#sigma_{#pi_{0}}, GeV"); //название осей
		grh[i]->GetYaxis()->SetTitleOffset(1.37);
		grh[i]->GetXaxis()->SetTitle("pT_{#gamma#gamma}, GeV/c"); 
		grh[i]->GetXaxis()->SetRangeUser(downPt-0.5, downPt+binMult*binWidth +0.5);
		grh[i]->GetYaxis()->SetRangeUser(0.0015,0.008);

		if(i==0) grh[i]->Draw("AEP");
		else grh[i]->Draw("sameE");
	}
	
	for (int i = 0; i < pidType.nType; ++i)
		grh[i]->Draw("samePX");

	Double_t pos[2] = {0.15, 0.86};
	MakeStandartLabelForPublicationMinimal( pos, true, f->GetName(), MakeCanvasDatasetLabel() );

	Double_t pos2[2] = {0.43, 0.15};
	MakeSimplLabel( pos2, "ALICE work in progress" );

	TLegend *leg2 = new TLegend(0.8,0.58,0.90,0.72);
	leg2->SetFillStyle(0);
	leg2->SetBorderSize(0);
	for (int i = 0; i < 4; ++i)
	{
		leg2->AddEntry(grh[i], pidType.canvasName[i],"lp");
	}
	leg2->Draw("same");

	ChangeDirectory( string(systemFile->GetTitle()), "", string(stTargetFile.Data()) );
	string pictureNameSigma = "MassInvSigmaVsPtTrig.png";
	canvasSigma->SaveAs(pictureNameSigma.c_str());
	// Возвращаемся в исходную папку.
	gSystem->cd( systemFile->GetTitle() );


	// Делаем фитирование сигмы по Both2Core
	TString sFPtSigma = "TMath::Sqrt([0]*[0]/x+[1]*[1]/x/x+[2]*[2])";
	TF1 *fit_pt_sigma = new TF1("funcSigmaPt",sFPtSigma.Data(), downPt, downPt+binMult*binWidth );
	fit_pt_sigma->SetParameters(0.000005,0.008,0.004);
	fit_pt_sigma->SetParLimits(0, -0.01, 0.01);
	fit_pt_sigma->SetParLimits(1, -0.01, 0.01);
	fit_pt_sigma->SetParLimits(2, -0.005, 0.005);
	grh[3]->Fit(fit_pt_sigma,"RQ");

	ChangeDirectory( string(systemFile->GetTitle()), "", string(stTargetFile.Data()), "Mass", "Fit" );
	canvasSigma->SaveAs(pictureNameSigma.c_str());
	canvasSigma->Close();
	// Возвращаемся в исходную папку.
	gSystem->cd( systemFile->GetTitle() );


	WriteFitParametrsToFile( systemFile, f, fit_pt_mass, fit_pt_sigma );


	if(flag.Contains("C_GET_FIT_MASS"))  return fit_pt_mass;
	if(flag.Contains("C_GET_FIT_SIGMA")) return fit_pt_sigma;

	if(!flag.Contains("F_USE_CURRENT_FILE")) f->Close();

	return 0x0;
}



void WriteFitParametrsToFile(const TSystemFile* systemFile, TFile* f, TF1* fit_pt_mass, TF1* fit_pt_sigma)
{
	/*	Пишет в файл ../Mass/Fit/data.txt параметры фитов зависимости положения пика массы и его ширины от pT.
	*
	*	systemFile 		- указатель на файл
	*	f 				- указатель на открытый root файл
	*	fit_pt_mass   	- фит распределения положения пика масс от pT
	*	fit_pt_sigma	- фит распределения ширины пика масс от pT
	*/

	TString stTargetFile = f->GetName();
	CutRootPart(stTargetFile);
	CutMergePartFromDataSet(stTargetFile);

	//Открываем файл с данными и пишем туда значения параметров фита.
	ChangeDirectory( string(systemFile->GetTitle()), "", string(stTargetFile.Data()), "Mass", "Fit" );
	std::ofstream fout("data.txt");
	//Пишем в него.

	if(MakeCanvasDatasetLabel().Contains("PbPb") ) 
	fout<<"PbPb ";
	else 
	fout<<"pPb ";
	fout<<MakeCanvasCentralityLabel(f->GetName())<<endl;

	fout<<"Double_t meanParametrs[2]  = {"	<<fit_pt_mass->GetParameter(0)<<", "
										<<fit_pt_mass->GetParameter(1)<<" };"<<endl;

	fout<<"Double_t sigmaParametrs[3] = {"	<<fit_pt_sigma->GetParameter(0)<<", "
										<<fit_pt_sigma->GetParameter(1)<<", "
										<<fit_pt_sigma->GetParameter(2)<<" };"<<endl;

	fout.close();
}