FhMakeMassParametrizationTexTable ( const TObjArray* fileListIncome, const TString flag )
{
	/*	Составляет таблицу по результатам фитирования зависимостей положения пика массы триггерной частицы в зависимости от pT.
		Только для PID: Both2qore
	*	fileList 		- лист файлов systemFile
	* 	flag 			- управление различными сценариями

	*	Сохраняет таблицу в massFit.tex файл в общей папке датасетов данного типа столкновения.
	*/

	PrintInTerminal_Yellow("Building Tex Tables for mean and sigma pT dependence."); 

	TObjArray* fileList = SortFileListByCentrality_Default(fileListIncome);
	
	// Переходим в директорию датасета.
	TSystemFile* systemFile = (TSystemFile*)fileList->At(0);
	gSystem->cd( systemFile->GetTitle() );
	// Определяем датасет.
	TString dataset, dataText, datasetLabel;
	if(TString(systemFile->GetTitle()).Contains("PbPb") ) 
	{
		datasetLabel = "PbPb";
		dataset  = "Pb-Pb";
		dataText = "LHC11h";
	}
	else 
	{
		datasetLabel = "pPb";
		dataset  = "p-Pb";
		dataText = "LHC13 bcdef";
	}


	// Открываем файл tex для записи.
	std::ofstream fout("massFit.tex");
	// Пишем в него шапку датасета.
	fout<<"\\begin{table}[h!]"<<endl;
	fout<<Form("\\caption{Параметры фитирования зависимости Mean и $\\sigma$ измеренных $\\pi^0$-мезонов в %s столкновениях.}", dataset.Data())<<endl;
	fout<<Form("\\label{tabl:mass_fit_%s}", datasetLabel.Data())<<endl;
	fout<<"\\begin{center}"<<endl;
	fout<<"\\begin{tabular}{|c|c|c|}"<<endl;
	fout<<"\\hline"<<endl;

	// Пишем в шапку датасет.
	fout<<Form("\\multicolumn{3}{|c|}{%s} \\\\", dataText.Data())<<endl;

	fout<<"\\hline"<<endl;
	fout<<"Centrality & Mean & Sigma \\\\"<<endl;
	fout<<"\\hline"<<endl;

	// Формируем по ячейкам центральности строки и выводим их.
	Bool_t taskExist = false;
	Pi0GlobalInfo pi0GlobalInfo; // Пустая. В FhMassInvVsPtTriger осталось это поле на случай расширения функционала.

	for (int k = 0; k<fileList->GetEntries(); k++)
	{
		systemFile = (TSystemFile*)fileList->At(k);
		TString name = systemFile->GetName();

		if(TString(systemFile->GetName()).Contains("Merge")) 
		{
			PrintInTerminal_Green(Form("Skip %s as merge file.", systemFile->GetName()));
			continue;
		}

		PrintInTerminal_Yellow(Form("Analise %s", name.Data() ));

		// Задаём число бинов и стартовый импульс для масс в pPb и PbPb соответвственно.
		InfoTransfer transfer = GetMassBining(systemFile);

		Int_t minPtValue 		= transfer.mean ;
		Int_t binMultiplicity   = transfer.intreal ;
		Int_t binWidth			= transfer.intmix ;

		const TFile* f = OpenFile(systemFile, flag);
		gSystem->cd( systemFile->GetTitle() );

		TF1* fit_mean  =  FhMassInvVsPtTriger( systemFile, pi0GlobalInfo, " F_USE_CURRENT_FILE C_GET_FIT_MASS",  binMultiplicity, binWidth, minPtValue );
		TF1* fit_sigma =  FhMassInvVsPtTriger( systemFile, pi0GlobalInfo, " F_USE_CURRENT_FILE C_GET_FIT_SIGMA", binMultiplicity, binWidth, minPtValue );

		// Пишем данные.
		fout<<Form(" 		& $p_0$: $(%.0e) \\pm (%.0e)$ & $p_0$: $(%.0e) \\pm (%.0e)$ \\\\", fit_mean->GetParameter(0), fit_mean->GetParError(0), fit_sigma->GetParameter(0), fit_sigma->GetParError(0))<<endl;
		fout<<Form(" 	%s	& $p_1$: $(%.4f) \\pm (%.0e)$ & $p_1$: $(%.0e) \\pm (%.0e)$ \\\\", MakeCanvasCentralityLabelForTex(name).Data(), fit_mean->GetParameter(1), fit_mean->GetParError(1), fit_sigma->GetParameter(1), fit_sigma->GetParError(1))<<endl;						
		fout<<Form(" 		& & $p_2$: $(%.0e) \\pm (%.0e)$ \\\\", fit_sigma->GetParameter(2), fit_sigma->GetParError(2))<<endl;
		fout<<"\\hline"<<endl;	

		f->Close();
	}

	fout<<"\\end{tabular}"<<endl;
	fout<<"\\end{center}"<<endl;
	fout<<"\\end{table}"<<endl;

	PrintInTerminal_Green("File massFit.tex done.");
	fout.close();

	PrintInTerminal_Green("End of analise!");
}