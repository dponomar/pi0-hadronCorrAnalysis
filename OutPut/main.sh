#!/bin/bash
# $1 - run's collection name ( ex. 169589AOD )
startM=$SECONDS # Начало выполнения скрипта в секундах.

# Минимальный и максимальный импульс триггерной частицы
# (задаётся вручную и основывается на результатах спекстра масс в StartAnalise2Mass().)
Pi0Min=$2
Pi0Max=$3

echo "$(tput setaf 2)Пределы по импульсам триггерных частиц: "
echo "           Pi0Min - $Pi0Min"
echo "           Pi0Max - $Pi0Max"
echo "Info: Пределы всегда можно поменять в analise.sh основываясь на резултатах StartAnalise2Mass()"
echo "$(tput sgr0)"

# Start
#============================================================================

echo "$(tput setaf 3)StartAnalise1MainInfo.C ( $1, $Pi0Min, $Pi0Max ) $(tput sgr0)";				root -l -b -q StartAnalise1MainInfo.C\(\"$1\"\,\"$Pi0Min\"\,\"$Pi0Max\"\)

echo "$(tput setaf 2)================================================================================================================$(tput sgr0)";
echo "$(tput setaf 3)StartAnalise2Mass.C ( $1, $Pi0Min, $Pi0Max ) $(tput sgr0)"; 					root -l -b -q StartAnalise2Mass.C\(\"$1\"\,\"$Pi0Min\"\,\"$Pi0Max\"\)

echo "$(tput setaf 2)================================================================================================================$(tput sgr0)";
echo "$(tput setaf 3)StartAnalise3PhiDistribution.C ( $1, $Pi0Min, $Pi0Max ) $(tput sgr0)"; 		root -l -b -q StartAnalise3PhiDistribution.C\(\"$1\"\,\"$Pi0Min\"\,\"$Pi0Max\"\)

echo "$(tput setaf 2)================================================================================================================$(tput sgr0)";
echo "$(tput setaf 3)StartAnalise4PhiEta.C ( $1, $Pi0Min, $Pi0Max ) $(tput sgr0)"; 					root -l -b -q StartAnalise4PhiEta.C\(\"$1\"\,\"$Pi0Min\"\,\"$Pi0Max\"\)

echo "$(tput setaf 2)================================================================================================================$(tput sgr0)";
echo "$(tput setaf 3)StartAnalise5Yield.C ( $1, $Pi0Min, $Pi0Max ) $(tput sgr0)"; 					root -l -b -q StartAnalise5Yield.C\(\"$1\"\,\"$Pi0Min\"\,\"$Pi0Max\"\)



echo "$(tput setaf 2)================================================================================================================$(tput sgr0)";
echo "$(tput setaf 3)StartAnalise6Yield_ICP.C ( $1, $Pi0Min, $Pi0Max ) $(tput sgr0)"; 				root -l -b -q StartAnalise6Yield_ICP.C\(\"$1\"\,\"$Pi0Min\"\,\"$Pi0Max\"\)

echo "$(tput setaf 2)================================================================================================================$(tput sgr0)";
echo "$(tput setaf 3)StartAnalise7Compare.C ( $1, $Pi0Min, $Pi0Max ) $(tput sgr0)"; 				root -l -b -q StartAnalise7Compare.C\(\"$1\"\,\"$Pi0Min\"\,\"$Pi0Max\"\)
echo "$(tput setaf 3)================================================================================================================$(tput sgr0)";

echo "$(tput setaf 2)================================================================================================================$(tput sgr0)";
echo "$(tput setaf 3)StartAnalise8MakeTexTables.C ( $1, $Pi0Min, $Pi0Max ) $(tput sgr0)"; 			root -l -b -q StartAnalise8MakeTexTables.C\(\"$1\"\,\"$Pi0Min\"\,\"$Pi0Max\"\)
echo "$(tput setaf 3)================================================================================================================$(tput sgr0)";

# Конец выполнения скрипта. Считаем потраченное время и выводим на экран.
durationMain=$(( SECONDS - startM ))

echo "$(tput setaf 6)"
echo "Анализ скриптом analise.sh ($1) завершён."
echo "Времени потрачено в main.sh: $durationMain (sec)"
echo "$(tput sgr0)"

# Notification in KDE
notify-send "main.sh" "Task for $1, $Pi0Min<pT<$Pi0Max Gev/c successfuly finished"