#!/bin/bash
# $1 - run's collection name ( ex. 169589AOD )
start=$SECONDS # Начало выполнения скрипта в секундах.

red='\033[0;33m'
green='\033[0;32m'
NC='\033[0m' # No Color

# Start
#============================================================================

echo "$(tput setaf 3)StartDebug.C ( ) $(tput sgr0)";	root -l StartDebug.C


#============================================================================
# Конец выполнения скрипта. Считаем потраченное время и выводим на экран.
duration=$(( SECONDS - start ))

echo "$(tput setaf 6)"
echo "Анализ скриптом debugTPC.sh ($1, $2) завершён."
echo "Времени потрачено: $duration (sec)"
echo "$(tput sgr0)"