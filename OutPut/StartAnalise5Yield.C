#include <Scripts/GetRunInfo.C>

#include <Scripts/Yield/FhPerTrigerYieldvsPtAssoc.C>
#include <Scripts/Yield/FhYieldNsAsPublication.C>


void StartAnalise5Yield(const string run = "pPb195482AOD139", const TString pi0min = "7", const TString pi0max = "20")
{
	StartAnalise(run, pi0min, pi0max );
}


Calculate(	const TSystemFile * systemFile, 
			const Double_t ptPi0min = 7., 
			const Double_t ptPi0max = 20.)
// Запуск всех функций. Настоятельно рекомендую удалить пустые root файлы из рабочей директории и запастись кофе с печеньками.
{
	PrintInTerminal_Yellow(Form("Correlations Pi0's pT range is from %2.1f to %2.1f", ptPi0min, ptPi0max));

	const Pi0GlobalInfo pi0GlobalInfo;
	pi0GlobalInfo.ptMin = ptPi0min;
	pi0GlobalInfo.ptMax = ptPi0max;

	if( !(TString(systemFile->GetName()).Contains("Default")) ) continue;
	if( !TestRootFilieQuality(systemFile) )						continue;

	// if( !(TString(systemFile->GetName()).Contains("20t40")) ) continue;

	// FhPerTrigerYieldvsPtAssoc( systemFile, pi0GlobalInfo, " C_FOR_NEAR C_YIELD_BY_FULL_V2V3FREE С_CORR_CONT_SUM");
	// FhPerTrigerYieldvsPtAssoc( systemFile, pi0GlobalInfo, " C_FOR_AWAY C_YIELD_BY_FULL_V2V3FREE С_CORR_CONT_SUM");

	// FhPerTrigerYieldvsPtAssoc( systemFile, pi0GlobalInfo, " C_FOR_NEAR C_YIELD_BY_FULL_V2V3 С_CORR_CONT_SUM");
	// FhPerTrigerYieldvsPtAssoc( systemFile, pi0GlobalInfo, " C_FOR_AWAY C_YIELD_BY_FULL_V2V3 С_CORR_CONT_SUM");

	// FhPerTrigerYieldvsPtAssoc( systemFile, pi0GlobalInfo, " C_FOR_NEAR C_YIELD_BY_HIST_INT ");
	// FhPerTrigerYieldvsPtAssoc( systemFile, pi0GlobalInfo, " C_FOR_AWAY C_YIELD_BY_HIST_INT ");

	if( TString(systemFile->GetTitle()).Contains("pPb") )
		FhYieldNsAsPublication( systemFile, pi0GlobalInfo, " C_YIELD_BY_FULL_V2V3FREE С_CORR_CONT_SUM" ); 
	if( TString(systemFile->GetTitle()).Contains("PbPb") )
		FhYieldNsAsPublication( systemFile, pi0GlobalInfo, " C_YIELD_BY_FULL_V2V3FREE " ); 
}