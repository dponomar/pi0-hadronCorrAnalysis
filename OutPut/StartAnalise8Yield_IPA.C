#include <Scripts/GetRunInfo.C>

#include <Scripts/Yield/FhPerTrigerYieldvsPtAssoc_IPA.C>
// #include <StartAnalise7Yield_ICP.C>


void StartAnalise8Yield_IPA(const string run1 = "pPb1", const string run2= "pPb2", const TString pi0min = "7", const TString pi0max = "20")
{	
	Double_t ptPi0min = pi0min.Atoi(); 	
	Double_t ptPi0max = pi0max.Atoi();

	TObjArray* fileList1 = GetFileList(run1);
	TObjArray* fileList2 = GetFileList(run2);

	Calculate_IPA( fileList1, fileList2, ptPi0min, ptPi0max );
}


Calculate_IPA(	const TObjArray* fileList1, 
				const TObjArray* fileList2, 
				const Double_t ptPi0min = 7., 
				const Double_t ptPi0max = 20. )
{
	PrintInTerminal_Yellow(Form("I_PA Pi0's pT range is from %2.1f to %2.1f", ptPi0min, ptPi0max));
	const Pi0GlobalInfo pi0GlobalInfo;
	pi0GlobalInfo.ptMin = ptPi0min;
	pi0GlobalInfo.ptMax = ptPi0max;

	Bool_t taskExist = false;

	TSystemFile* systemFile_k; // i
	TSystemFile* systemFile_l; // j

	for (int k = 0; k<fileList1->GetEntries(); k++)
		for (int l = 0; l<fileList2->GetEntries(); l++)
		{
			systemFile_k = (TSystemFile*)fileList1->At(k);
			systemFile_l = (TSystemFile*)fileList2->At(l);

			TString name_k = systemFile_k->GetName();
			TString name_l = systemFile_l->GetName();

			if (name_k.Contains("NoEff") || name_l.Contains("NoEff")) continue;
			if (name_k.Contains("Left")  || name_l.Contains("Left"))  continue;
			if (name_k.Contains("Right") || name_l.Contains("Right")) continue;
			if (!TestRootFilieQuality(systemFile_k)) continue;
			if (!TestRootFilieQuality(systemFile_l)) continue;
			
			if (GetSigmaNumber(name_k) != GetSigmaNumber(name_l) ) continue; 									// выбираем только с одинаковыми сигма
			if (GetCentralityUpBorder(name_k) != GetCentralityUpBorder(name_l) ) 
				continue; 																						// отбираем только с одинаковыми центральностями


			taskExist = true;																					// Делаем метку что что-то отобрали.

			PrintInTerminal_Yellow	(	Form("Processing Calculate_IPA for ( %s %s )/( %s %s )", 
									GetLastFolderFromPass(systemFile_k->GetTitle()).Data(), systemFile_k->GetName(), 
									GetLastFolderFromPass(systemFile_l->GetTitle()).Data(), systemFile_l->GetName() )
									);
			FhPerTrigerYieldvsPtAssoc_IPA( systemFile_k, systemFile_l, pi0GlobalInfo, "C_FOR_NEAR C_YIELD_BY_FIT" );
			FhPerTrigerYieldvsPtAssoc_IPA( systemFile_k, systemFile_l, pi0GlobalInfo, "C_FOR_AWAY C_YIELD_BY_FIT" );
		}

	if (!taskExist) PrintInTerminal_Red("No roots file with different centrality.");
		
	PrintInTerminal_Green("End of analise!");		
}
