#include <Scripts/GetRunInfo.C>

#include <Scripts/MainInfo/FhCentralityX.C>
#include <Scripts/MainInfo/FhSingleClusters.C>
#include <Scripts/MainInfo/FhTotSelEvents.C>
#include <Scripts/MainInfo/FhPassedCutsEvents.C>
#include <Scripts/MainInfo/FhTrackPhiEta.C>
#include <Scripts/MainInfo/FhTrackPhiEtaXY.C>
#include <Scripts/MainInfo/FhPHOScluXZE.C>
#include <Scripts/MainInfo/FhClusterMultiplicity.C>
#include <Scripts/MainInfo/FhMAssWindow.C>
#include <Scripts/MainInfo/FhPhiRPflat.C>
#include <Scripts/MainInfo/FhMixingBining_RP.C>
#include <Scripts/MainInfo/FhMixingBining_Centrality.C>
#include <Scripts/MainInfo/FhMixingBining_VertexZ.C>

#include <Scripts/MainInfo/FhTrigerParticlesYieldvsPtTrigger.C>
#include <Scripts/MainInfo/FhAssociatedParticlesYieldvsPtAssoc.C>



void StartAnalise1MainInfo(const string run = "pPb195482AOD139", const TString pi0min = "7", const TString pi0max = "20")
{	
	StartAnalise(run, pi0min, pi0max );
}


Calculate(	const TSystemFile * systemFile, 
			const Double_t ptPi0min = 7., 
			const Double_t ptPi0max = 20.)
// Запуск всех функций. Настоятельно рекомендую удалить пустые root файлы из рабочей директории и запастись кофе с печеньками.
{
	PrintInTerminal_Yellow(Form("Pi0 pT from %2.1f to %2.1f:", ptPi0min, ptPi0max));
	
	Pi0GlobalInfo pi0GlobalInfo;
	pi0GlobalInfo.ptMin = ptPi0min;
	pi0GlobalInfo.ptMax = ptPi0max;

	if(TString(systemFile->GetName()).Contains("Merge") || TString(systemFile->GetName()).Contains("LR")) 
	{
		PrintInTerminal_Green(Form("Skip %s as merge or LR file.", systemFile->GetName()));
		continue;
	}

	if(TString(systemFile->GetName()).Contains("NoEff"))
	{
		FhAssociatedParticlesYieldvsPtAssoc	( systemFile, pi0GlobalInfo, "" );
		FhTrigerParticlesYieldvsPtTrigger	( systemFile, pi0GlobalInfo, "" );
	}
		// else
		//if(TString(systemFile->GetName()).Contains("Default"))
	{
		FhTotSelEvents			( systemFile, ""		);
		FhPassedCutsEvents		( systemFile, ""		);
		FhCentralityX			( systemFile, "All" 	);
		FhCentralityX			( systemFile, "Trigger" );
		FhCentralityX			( systemFile, "MB" 		);

		FhSingleClusters		( systemFile, ""		);
		FhPHOScluXZE			( systemFile, ""		);

		FhTrackPhiEta			( systemFile 	 		);
		FhTrackPhiEtaXY			( systemFile, "X" 		);
		FhTrackPhiEtaXY			( systemFile, "Y" 		);

		FhClusterMultiplicity	( systemFile 			);
		FhMAssWindow			( systemFile 			);
		FhPhiRPflat				( systemFile 			);

		FhMixingBining_RP(systemFile );
		FhMixingBining_Centrality(systemFile);
		FhMixingBining_VertexZ(systemFile);
	}
}
