#!/bin/bash
start=$SECONDS # Начало выполнения скрипта в секундах.

cd "${ALICE_PHYSICS}/../build"
make -j$MJ install

#ln -nfs /opt/alice/aliroot/master/build/include "$ALICE_ROOT"/include

cd ~/Yandex.Disk/Master/NIRS/AdventureTime >& /dev/null

# Конец выполнения скрипта. Считаем потраченное время и выводим на экран.
duration=$(( SECONDS - start ))

echo "$(tput setaf 6)Recompile end's. Time: $duration (sec)$(tput sgr0)"

