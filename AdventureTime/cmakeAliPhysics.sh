#!/bin/bash
start=$SECONDS # Начало выполнения скрипта в секундах.

cd "$(dirname "$ALICE_PHYSICS")/build"
cmake "$(dirname "$ALICE_PHYSICS")/src" \
  -DCMAKE_INSTALL_PREFIX="$ALICE_PHYSICS" \
  -DCMAKE_C_COMPILER=`root-config --cc` \
  -DCMAKE_CXX_COMPILER=`root-config --cxx` \
  -DCMAKE_Fortran_COMPILER=`root-config --f77` \
  -DALIEN="$ALIEN_DIR" \
  -DROOTSYS="$ROOTSYS" \
  -DFASTJET="$FASTJET" \
  -DALIROOT="$ALICE_ROOT"
  
# cmake "$(dirname "$ALICE_ROOT")/src" \
#   -DCMAKE_C_COMPILER=`root-config --cc` \
#   -DCMAKE_CXX_COMPILER=`root-config --cxx` \
#   -DCMAKE_Fortran_COMPILER=`root-config --f77` \
#   -DCMAKE_MODULE_LINKER_FLAGS='-Wl,--no-as-needed' \
#   -DCMAKE_SHARED_LINKER_FLAGS='-Wl,--no-as-needed' \
#   -DCMAKE_EXE_LINKER_FLAGS='-Wl,--no-as-needed' \
#   -DCMAKE_INSTALL_PREFIX="$ALICE_ROOT" \
#   -DALIEN="$ALIEN_DIR" \
#   -DROOTSYS="$ROOTSYS" \
#   -DFASTJET="$FASTJET"

cd ~/Yandex.Disk/Master/NIRS/AdventureTime >& /dev/null

# Конец выполнения скрипта. Считаем потраченное время и выводим на экран.
duration=$(( SECONDS - start ))

echo "$(tput setaf 6)Cmake end's. Time: $duration (sec)$(tput sgr0)"