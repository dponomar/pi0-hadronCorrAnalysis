#ifndef AliPHOSCorrelations_cxx
#define AliPHOSCorrelations_cxx

/* Copyright(c) 1998-2015, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice     */

// Analysis task for pi0-hadron correlation whis PHOS detector.
// Author:  Daniil Ponomarenko <Daniil.Ponomarenko@cern.ch>
// 20-Feb-2015


class AliESDEvent ;
class AliAODInputHandler;
class AliStack ;

#ifndef ROOT_AliAnalysisTaskSE
#include "AliAnalysisTaskSE.h"
#endif

#ifndef ROOT_TArrayD
#include "TArrayD.h"
#endif

class AliPHOSCorrelations : public AliAnalysisTaskSE 
{
public:
  AliPHOSCorrelations() ;
  AliPHOSCorrelations(const char *name) ;

    AliPHOSCorrelations(const AliPHOSCorrelations& obj);
    AliPHOSCorrelations& operator=(const AliPHOSCorrelations& other);

  virtual ~AliPHOSCorrelations() ;

  virtual void   UserCreateOutputObjects() ;
  virtual void   UserExec(Option_t *option) ;

  void SetCentralityEstimator(Int_t centr)                                 { fCentralityEstimator = centr         ; }
  Int_t GetCentralityEstimator()                                            { return fCentralityEstimator          ; }


protected:
  //General Data members
  TList *   fOutputContainer ;                          //! Output histograms container 
  AliVEvent   *           fEvent;                       //! Current event
  Int_t   fCentralityEstimator;                       //! Centrality estimator ("V0M", "ZNA")

  ClassDef(AliPHOSCorrelations, 5);                     // PHOS analysis task
};

#endif

