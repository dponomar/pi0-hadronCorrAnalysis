/**************************************************************************
 * Copyright(c) 1998-2015, ALICE Experiment at CERN, All rights reserved. *
 *                                                                        *
 * Author: The ALICE Off-line Project.                                    *
 * Contributors are mentioned in the code where appropriate.              *
 *                                                                        *
 * Permission to use, copy, modify and distribute this software and its   *
 * documentation strictly for non-commercial purposes is hereby granted   *
 * without fee, provided that the above copyright notice appears in all   *
 * copies and that both the copyright notice and this permission notice   *
 * appear in the supporting documentation. The authors make no claims     *
 * about the suitability of this software for any purpose. It is          *
 * provided "as is" without express or implied warranty.                  *
 **************************************************************************/
 
// Analysis task for pi0-hadron correlation whis PHOS detector.
// Author:     Daniil Ponomarenko <Daniil.Ponomarenko@cern.ch>
// 20-Feb-2015

#include <Riostream.h>
#include "THashList.h"
#include "TObjArray.h"
#include "TClonesArray.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH2I.h"
#include "TH3F.h"
#include "TH3D.h"
#include "TMath.h"
#include "TVector3.h"
#include "TProfile.h"

#include "AliAnalysisManager.h"
#include "AliAnalysisTaskSE.h"
#include "AliPHOSCorrelations.h"

#include "AliAODEvent.h"

using std::cout;
using std::endl;

ClassImp(AliPHOSCorrelations)

//_______________________________________________________________________________
AliPHOSCorrelations::AliPHOSCorrelations()
:AliAnalysisTaskSE(),
    fOutputContainer(0x0),
    fEvent(0x0),
    fCentralityEstimator(1)
{
     // Constructor
}

//_______________________________________________________________________________
AliPHOSCorrelations::AliPHOSCorrelations(const char *name)
:AliAnalysisTaskSE(name),
    fOutputContainer(0x0),
    fEvent(0x0),
    fCentralityEstimator(1)
{
    // Constructor
    // Output slots 
    DefineOutput(1,THashList::Class());
}

AliPHOSCorrelations::AliPHOSCorrelations(const AliPHOSCorrelations& obj)
:AliAnalysisTaskSE(obj)
{
// Copy constructor
    fCentralityEstimator = obj.fCentralityEstimator;
}

AliPHOSCorrelations& AliPHOSCorrelations::operator=(const AliPHOSCorrelations& other)
{
// Assignment
  if(&other == this) return *this;
  AliAnalysisTaskSE::operator=(other);

    AliAnalysisTaskSE::operator=(other);
    fCentralityEstimator  = other.fCentralityEstimator;
   
    return *this;
}

//_______________________________________________________________________________
AliPHOSCorrelations::~AliPHOSCorrelations()
{
    if(fOutputContainer){
      delete fOutputContainer;
      fOutputContainer=0x0;
    }      
}

//_______________________________________________________________________________
void AliPHOSCorrelations::UserCreateOutputObjects()
{
    // Create histograms
    if(fOutputContainer != NULL) { delete fOutputContainer; }
    fOutputContainer = new THashList();
    fOutputContainer->SetOwner(kTRUE);

    PostData(1, fOutputContainer);
}


//_______________________________________________________________________________
void AliPHOSCorrelations::UserExec(Option_t *) 
{
    fEvent = InputEvent();
    if( ! fEvent ) 
    {
        AliError("Event could not be retrieved");
        PostData(1, fOutputContainer);
        return ;
    }

    cout<< fCentralityEstimator <<endl;

    PostData(1, fOutputContainer);
}